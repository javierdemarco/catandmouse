import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'RatonGato.settings')

import django
django.setup()

from server.models import Game, User, Move


def populate():
    
    u1 = add_User("paz", "paz123")
    u2 = add_User("javi", "javi456")
    u3 = add_User("QueridoJose", "AprobamosPSI")

    g1 = add_Game(u1, u2, "PazCazaJavi")

    add_Move(g1)

def add_User(username, password):
    u = User.objects.get_or_create(username=username, password=password)[0]
    u.save()
    return u
    
def add_Game(catUser, mouseUser, name):
    g = Game.objects.get_or_create(catUser=catUser)[0]
    g.mouseUser=mouseUser
    g.name=name
    g.save()
    return g

def add_Move(game):
    m = Move.objects.get_or_create(game=game.name)[0]
    m.save()
    return m

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()


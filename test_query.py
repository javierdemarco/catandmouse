import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'RatonGato.settings')

import django
django.setup()

from server.models import User, Game, Move

#Funciones aux


def test_query():
    #Comprobar si existe usu con id=10
    id = 10
    username = 'u10'
    password = 'p10'

    u = User.objects.filter(id=id)
    if u.exists():
        print("El usuario con id = %d ya existe" % id)

    else:
        u10 = add_User(id, username, password)
        u10.save()

    #Comprobar si existe usu con id=11
    id = 11
    username = 'u11'
    password = 'p11'

    u = User.objects.filter(id=id)
    if u.exists():
        print("El usuario con id = %d ya existe" % id)

    else:
        u11 = add_User(id, username, password)
        u11.save()

    #Creacion de un juego con el segundo usuario NULL
    u10 = User.objects.get(username='u10')
    id = 0
    g1 = add_Game(id, u10, None, "JuegoTest")
    g = Game.objects.filter(id=g1.id)
    if g.exists():
        print("El juego con id = %d ya existe" % g1.id)
        g1 = None
    else:
        g1.save()
        print("Juego con id: %d creado" % g1.id)

    #Busqueda juego con un solo usuario
    for g in Game.objects.filter(mouseUser__isnull=True):
        print("Id de juegos con un solo usuario: %d" % g.id)

    #El usuario u11 se une al juego del apartado anterior.
    u11 = User.objects.get(username='u11')

    g = Game.objects.filter(mouseUser__isnull=True)
    if g.exists():
        print("Se anhade u11 al juego del apartado anterior")
        g1 = Game.objects.get(mouseUser__isnull=True)
        g1.mouseUser = u11
        g1.save()
    else:
        print("El juego que se creo en el apartado anterior ya tiene u11")

    #Comprobacion de la adicion
    for g in Game.objects.filter(mouseUser__isnull=True):
        print("Id de juegos con un solo usuario: %d" % g.id)

    #Usuario u11 hace un movimiento en el raton del 59 al 60
    u11 = User.objects.get(username="u11")
    g = Game.objects.get(name="JuegoTest")
    origin = 3
    target = 12
    move_resultado = add_Move(origin=origin, target=target, game=g, user=u11)
    print(move_resultado)

    #Usuario u10 hace un movimiento en el gato 1 del 0 al 1
    u10 = User.objects.get(username="u10")
    g = Game.objects.get(name="JuegoTest")
    origin = 62
    target = 55
    move_resultado = add_Move(origin=origin, target=target, game=g, user=u10)
    print(move_resultado)


def add_Game(id, catUser, mouseUser, name):
    g = Game(id=id, catUser=catUser, mouseUser=mouseUser, name=name)
    return g


def add_User(id, username, password):
    u = User(id=id, username=username, password=password)
    return u


def add_Move(origin, target, game, user):
    m = Move(origin=origin, target=target, game=game.name)
    if((user.id == game.catUser.id) and (game.catTurn == 1)):
        if(origin == game.cat1):
            game.cat1 = target
            game.catTurn = -1
            game.save()
            m.save()
            return "Movimiento Realizado"
        if(origin == game.cat2):
            game.cat2 = target
            game.catTurn = -1
            game.save()
            m.save()
            return "Movimiento Realizado"
        if(origin == game.cat3):
            game.cat3 = target
            game.catTurn = -1
            game.save()
            m.save()
            return "Movimiento Realizado"
        if(origin == game.cat4):
            game.cat4 = target
            game.catTurn = -1
            game.save()
            m.save()
            return "Movimiento Realizado"
    if((user.id == game.mouseUser.id) and (game.catTurn == -1)):
        if(origin == game.mouse):
            game.mouse = target
            game.catTurn = 1
            game.save()
            m.save()
            return "Movimiento Realizado"
    return "No es tu turno de jugar"

# Start execution here!
if __name__ == '__main__':
    print("Starting Rango Test Query script...")
    test_query()

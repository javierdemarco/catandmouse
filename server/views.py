from django.shortcuts import render
from server.models import User
from django.template import RequestContext
from django.http import HttpResponse
from server.forms import RegistroForm, LoginForm, AnhadirJuegoForm

# Create your views here.
def index(request):
    return render(request, 'server/index.html')

def add_user(request):
    # A HTTP POST?
    if request.method == 'POST':
        form = RegistroForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
            form.save(commit=True)

            # Now call the index() view.
            # The user will be shown the homepage.
            return correct_user(request)
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
        form = RegistroForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'server/add_user.html', {'form': form})

def correct_user(request):
	return HttpResponse("ENHORABUENA! Resgistro correcto.")

def login_user(request):
    statsDict={}
    if request.method == 'POST':
	login_user(request)
    else:
        # If the request was not a POST, display the form to enter details.
        form = LoginForm()
        error = ""

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'server/login.html', {'form': form, 'error': error})


def add_game(request):
    statsDict = {}
    if request.method == 'POST':
        add_game(request)
    else:
        # If the request was not a POST, display the form to enter details.
        form = AnhadirJuegoForm()
        error = ""

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'server/add_game.html', {'form': form, 'error': error})

def game(request):
    return render(request, 'server/game.html')

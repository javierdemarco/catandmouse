# -*- coding: utf-8 -*-
from tastypie.resources import ModelResource, ALL
from models import User, Game, Move   #clase donde se almacenan los usuarios del sistema

from django.conf.urls import url
import math
from tastypie.utils import trailing_slash

#SOBRABA
#from requests import Request, Session
#from django.contrib.auth import authenticate, login
#from django.http import HttpResponseRedirect, HttpResponse
#import urllib2, urllib, sys

#reload(sys)
#sys.setdefaultencoding('utf8')


class UserResource (ModelResource):
    """good_for_search_http://127.0.0.1:8000/server/api/user/?id=111&format
    =json"""
    class Meta:
        queryset = User.objects.all()
        #Este resorce -conjunto de webservices- se identifica con la etiqueta user
        resource_name = "user"
        #las siguientes lineas no son relevantes para nuestro juego
        #informan al servidor de que autorice
        #busquedas tanto por id como por username
        filtering = {
            'id': ALL,  #FALTABA identación
            'userName': ALL  #FALTABA identación
        }
        #agnade al mapeo de urls los webservices que desarrolleis

    def prepend_urls(self):
        return [

            #url(r"ˆ(%s)/add_user%s$" %(self._meta.resource_name, trailing_slash()),
            #self.wrap_view('add_user'), name="api_add_user"),

            #url(r"ˆ(%s)/login_user%s$" %(self._meta.resource_name, trailing_slash()),
            #self.wrap_view('login_user'), name="api_login_user"),

            #url(r"ˆ(%s)/add_game%s$" %(self._meta.resource_name, trailing_slash()),
            #self.wrap_view('add_game'), name="api_add_game"),

            #url(r"ˆ(%s)/join_game%s$" %(self._meta.resource_name, trailing_slash()),
            #self.wrap_view('join_game'), name="api_join_game"),

            #url(r"ˆ(%s)/move%s$" %(self._meta.resource_name, trailing_slash()),
            #self.wrap_view('move'), name="api_move"),

            #url(r"ˆ(%s)/status%s$" %(self._meta.resource_name, trailing_slash()),
            #self.wrap_view('status'), name="api_status"),

            url(r"^(%s)/counter%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('counter'), name="api_counter"),

            url(r"^(%s)/add_user%s$" % (self._meta.resource_name,
                trailing_slash()), self.wrap_view('add_user'), name="api_add_user"),

            url(r"^(%s)/login_user%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login_user'), name="api_login_user"),

            url(r"^(%s)/add_game%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_game'), name="api_add_game"),

            url(r"^(%s)/clean_orphan_games%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('clean_orphan_games'), name="api_clean_orphan_games"),

            url(r"^(%s)/join_game%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('join_game'), name="api_join_game"),

            url(r"^(%s)/move%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('move'), name="api_move"),

            url(r"^(%s)/status%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('status'), name="api_status"),
        ]

    def counter(self, request, *args, **kwargs):
        statsDict = {}
        if('counter' in request.session):
            request.session['counter'] += 1
            statsDict['value'] = request.session['counter']
            statsDict['result'] = True
            statsDict['message'] = "Counter aumentado correctamente."

        else:
            request.session['counter'] = 1
            statsDict['value'] = request.session['counter']
            statsDict['result'] = True
            statsDict['message'] = "Counter seteado correctamente."
        return self.create_response(request, statsDict)

    def add_user(self, request, *args, **kwargs):
        statsDict = {}
        username = request.POST['username']
        password = request.POST['password']
        try:
            u = User(password=password, username=username)
            u.save()
            request.session['user_id'] = u.id
            statsDict['value'] = u.id
            statsDict['result'] = True
            statsDict['message'] = "Usuario aniadido correctamente."
        except Exception as e:
            statsDict['value'] = -1
            statsDict['message'] = e.message
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def login_user(self, request, *args, **kwargs):
        statsDict = {}
        try:
            username = request.POST['username']
            u = User.objects.get(username=username)
            password = request.POST['password']
            if(u.password == password):
                request.session['user_id'] = u.id
                statsDict['value'] = u.id
                statsDict['result'] = True
                statsDict['message'] = "Usuario logeado correctamente."
            else:
                statsDict['value'] = u.id
                statsDict['result'] = False
                statsDict['message'] = "Usuario encontrado. Password incorrecta."
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def add_game(self, request, *args, **kwargs):
        statsDict = {}
        try:
            u = User.objects.get(id=request.session['user_id'])
            game = Game(catUser=u, mouseUser=None)
            game.save()
            request.session['game_id'] = game.id
            statsDict['value'] = game.id
            statsDict['result'] = True
            statsDict['message'] = "Juego creado correctamente."
            request.session['amIcat'] = 1
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def clean_orphan_games(self, request, *args, **kwargs):
        statsDict = {}
        g = None
        try:
            statsDict['value'] = 0
            for g in Game.objects.filter(mouseUser__isnull=True):
                g.delete()
                statsDict['value'] += 1
            statsDict['result'] = True
            statsDict['message'] = "Juegos eliminados correctamente correctamente."
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def join_game(self, request, *args, **kwargs):
        statsDict = {}
        try:
            id = request.session['user_id']
            u = User.objects.get(id=id)
            g = Game.objects.filter(mouseUser_id__isnull=True)
            if(g is None):
                raise ValueError("No hay juegos huerfanos")
            game = g[0]
            game.mouseUser = u
            game.save()
            request.session['game_id'] = game.id
            request.session['amIcat'] = -1
            statsDict['value'] = game.id
            statsDict['result'] = True
            statsDict['message'] = "Se ha aniadido el usuario de sesion a un juego huerfano."
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def isValidCatMove(self, cm, origin, target):
        if(cm.cat1 == target or
            cm.cat2 == target or
            cm.cat3 == target or
            cm.cat4 == target):
                return False
        if not(cm.cat1 == origin or
                cm.cat2 == origin or
                cm.cat3 == origin or
                cm.cat4 == origin):
                    return False
        if not((origin - 7) == target or (origin - 9) == target):
                return False
        if not(cm.catTurn == 1):
            return False
        return True

    def cat_move(self, request, *args, **kwargs):
        move = int(request.POST['click'])
        statsDict = {}
        if not('game_id' in request.session):
            statsDict['message'] = "Crea un juego antes."
            statsDict['value'] = -1
            statsDict['result'] = False
            return self.create_response(request, statsDict)
        if not('origin' in request.session):
            request.session['origin'] = move
            statsDict['message'] = " "
            statsDict['value'] = 0
            statsDict['result'] = True
            return self.create_response(request, statsDict)
        try:
            game = Game.objects.get(id=request.session['game_id'])
            user = User.objects.get(id=request.session['user_id'])
            if not(game.catUser == user):
                statsDict['message'] = "No eres el gato."
                statsDict['value'] = -1
                statsDict['result'] = False
                return self.create_response(request, statsDict)
            if not(self.isValidCatMove(cm=game, origin=request.session['origin'], target=move)):
                statsDict['message'] = "El movimiento del gato no es valido."
                statsDict['value'] = -1
                statsDict['result'] = False
                del request.session['origin']
                return self.create_response(request, statsDict)
            m = Move(origin=request.session['origin'], target=move, game=game.name)
            m.save()
            if(request.session['origin'] == game.cat1):
                game.cat1 = move
            if(request.session['origin'] == game.cat2):
                game.cat2 = move
            if(request.session['origin'] == game.cat3):
                game.cat3 = move
            if(request.session['origin'] == game.cat4):
                game.cat4 = move
            game.catTurn = -1
            game.save()
            statsDict['message'] = "Movimiento del gato realizado."
            statsDict['value'] = m.id
            statsDict['result'] = True
            del request.session['origin']
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def isValidMouseMove(self, cm, target):
        if(cm.cat1 == target or
            cm.cat2 == target or
            cm.cat3 == target or
            cm.cat4 == target or
            cm.mouse == target):
                return False
        if not((cm.mouse - 7) == target or (cm.mouse - 9) == target
                or (cm.mouse + 7) == target or (cm.mouse + 9) == target):
                    return False
        if not(cm.catTurn == -1):
            return False
        return True

    def mouse_move(self, request, *args, **kwargs):
        statsDict = {}
        target = int(request.POST['click'])
        if not('game_id' in request.session):
            statsDict['message'] = "Crea un juego antes."
            statsDict['value'] = -1
            statsDict['result'] = False
            return self.create_response(request, statsDict)
        try:
            game = Game.objects.get(id=request.session['game_id'])
            user = User.objects.get(id=request.session['user_id'])
            if not(game.mouseUser == user):
                statsDict['message'] = "No eres el raton"
                statsDict['value'] = -1
                statsDict['result'] = False
                return self.create_response(request, statsDict)
            if not (self.isValidMouseMove(cm=game, target=target)):
                statsDict['message'] = "El movimiento del raton es incorrecto"
                statsDict['value'] = -1
                statsDict['result'] = False
                return self.create_response(request, statsDict)
            move = Move(origin=game.mouse, target=target, game=game.name)
            game.mouse = target
            game.catTurn = 1 
            game.save()
            move.save()
            statsDict['message'] = "El movimiento del raton es correcto"
            statsDict['value'] = move.id
            statsDict['result'] = True
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def move(self, request, *args, **kwargs):
        statsDict = {}
        try:
            if not('amIcat' in request.session):
                statsDict['message'] = "amIcat no seteado"
                statsDict['value'] = -1
                statsDict['result'] = False
            if(request.session['amIcat'] == 1):
                return self.cat_move(request=request)
            elif(request.session['amIcat'] == -1):
                return self.mouse_move(request=request)
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

    def status(self, request, *args, **kwargs):
        statsDict = {}
        type = request.POST['type']
        try:
            if type == 'cats':
                id = request.session['game_id']
                g = Game.objects.get(id=id)
                pos = ((2 ** g.cat1) | (2 ** g.cat2) | (2 ** g.cat3) | (2 ** g.cat4))
                statsDict['message'] = 'Posiciones de los gatos'
                statsDict['value'] = pos
                statsDict['result'] = True
            elif type == 'mouse':
                id = request.session['game_id']
                g = Game.objects.get(id=id)
                pos = (2 ** g.mouse)
                statsDict['message'] = 'Posiciones de los ratones'
                statsDict['value'] = pos
                statsDict['result'] = True
            elif type == 'myTurn':
                id = request.session['game_id']
                g = Game.objects.get(id=id)
                if(g.catTurn == request.session['amIcat']):
                    pos = 1
                else:
                    pos = -1
                statsDict['message'] = 'turno actual'
                statsDict['value'] = pos
                statsDict['result'] = True
            else:
                statsDict['message'] = 'La opcion introducida no es correcta'
                statsDict['value'] = -1
                statsDict['result'] = False
        except Exception as e:
            statsDict['message'] = e.message
            statsDict['value'] = -1
            statsDict['result'] = False
        return self.create_response(request, statsDict)

#-*-coding: utf-8-*-
from django.conf.urls import patterns, url, include
from django.contrib import admin
from tastypie.api import Api
from api import UserResource
from server import views  #FALTABA
#import views

admin.autodiscover()
#user_api=Api(api_name='user')
user_api = Api(api_name='v1')  #FALTABA
user_api.register(UserResource())

urlpatterns = patterns('',
    #url(r'ˆ$', views.index, name='index'),
    url(r'^$', views.index, name='index'),  #FALTABA
    #url(r'ˆadd_user/$', views.add_user, name='add_user'),
    url(r'^add_user/$', views.add_user, name='add_user'),  #FALTABA
    url(r'^add_game/$', views.add_game, name='add_game'),  #FALTABA
    #url(r'ˆlogin_user/$', views.login_user, name='login_user'),
    url(r'^login_user/$', views.login_user, name='login_user'),  #FALTABA
    url(r'^game/$', views.game, name='game'),
    #url(r'ˆapi/user/', include(user_api.urls)), #agnadir los urls contenidas en api.Userresource.prepend_urls
    url(r'^api/', include(user_api.urls)),  #FALTABA
)

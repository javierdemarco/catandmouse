from django import forms
from server.models import User, Game


class RegistroForm(forms.ModelForm):

    username = forms.CharField(max_length=128, help_text=" Please enter username.", required=True)
    password = forms.CharField(widget=forms.PasswordInput(), min_length=2, help_text=" Please enter password.", required=True)

    # An inline class to provide additional information on the form.
    class Meta:
        # Provide an association between the ModelForm and a model
        model = User
        fields = ('username', 'password')


class LoginForm(forms.Form):

    username = forms.CharField(max_length=128, help_text=" Please enter username.", required=True)
    password = forms.CharField(widget=forms.PasswordInput(), min_length=2, help_text=" Please enter password.", required=True)
    # An inline class to provide additional information on the form.

    class Meta:
        # Provide an association between the ModelForm and a model
        model = User
        fields = ('username', 'password')


class AnhadirJuegoForm(forms.Form):

    name = forms.CharField(max_length=128, help_text=" Please enter name.", required=False)
    # An inline class to provide additional information on the form.

    class Meta:
        # Provide an association between the ModelForm and a model
        model = Game
        fields = ('name')

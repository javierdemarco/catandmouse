from django.contrib import admin

# Register your models here.
from server.models import User, Game, Move

# Add in this class to customized the Admin Interface
class UserAdmin(admin.ModelAdmin):
    list_display=('userame', 'password')

class GameAdmin(admin.ModelAdmin):
    list_display=('catUser', 'mouseUser', 'cat1', 'cat2', 'cat3', 'cat4', 'mouse',
'catTurn', 'name')

class MoveAdmin(admin.ModelAdmin):
    list_display=('origin', 'target', 'game')
# Update the registeration to include this customised interface
admin.site.register(User)
admin.site.register(Game)
admin.site.register(Move)

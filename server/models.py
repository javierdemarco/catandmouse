from django.db import models

# Create your models here.


class User(models.Model):
    username = models.CharField(max_length=128, unique=True)
    password = models.CharField(max_length=128)

    def __unicode__(self):   #For Python 2, use __str__ on Python 3
        return self.username


class Game(models.Model):
        catUser = models.ForeignKey(User, related_name='game_catUsers')
        mouseUser = models.ForeignKey(User, related_name='game_mouseUser',
                                      null=True)
        cat1 = models.IntegerField(default=56)
        cat2 = models.IntegerField(default=58)
        cat3 = models.IntegerField(default=60)
        cat4 = models.IntegerField(default=62)
        mouse = models.IntegerField(default=3)
        catTurn = models.IntegerField(default=1)
        name = models.CharField(max_length=128, default="juego")

        def __unicode__(self):
                return self.name


class Move(models.Model):
        origin = models.IntegerField(default=0)
        target = models.IntegerField(default=2)
        game = models.CharField(max_length=128)

        def __unicode__(self):
                return self.game

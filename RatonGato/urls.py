# -*- coding: utf-8 -*- 

## from django.conf.urls import include, url, patterns
## from django.contrib import admin
## from server import urls


## urlpatterns = patterns('',
##     url(r'^admin/', include(admin.site.urls)),
## 	url(r'^server/', include('server.urls')),
## )

#CORREGIDO
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^server/', include('server.urls')),
]
